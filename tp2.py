class Box:
	def __init__(self, is_open=False, capacity=None):
		self._contents = []
		self._open = is_open
		self._capacity = capacity
	
	@staticmethod
	def from_yaml(data):
		isopen = data[0].get('is_open')
		capaci = data[0].get('capacity')
		return Box(is_open=isopen, capacity=capaci)
	
	def __repr__(self):
		if self.is_open():
			return "<capacité:%s; %s>" % (self.capacity(), self._contents)
		else:
			return "Boite fermee"
	
	def add(self, truc):
		self._contents.append(truc)

	def __contains__(self, machin):
		return machin in self._contents

	def remove(self, element):
		retire = False
		for i in range(len(self._contents)):
			if not retire:
				if self._contents[i] == element:
					retire = True
					del self._contents[i]
	
	def is_open(self):
		return self._open
	
	def close(self):
		self._open = False
	
	def open(self):
		self._open = True
	
	def action_look(self):
		if self.is_open(): # Si la boite est ouverte
			msg = "la boite contient:"
			for i in range(len(self._contents)):
				print(self._contents[i])
				msg += " "+ str(self._contents[i])
				if i != len(self._contents)-1:
					msg += ","
			return msg
		else:
			return "la boite est fermee"
	
	def set_capacity(self, cap):
		self._capacity = cap
	
	def capacity(self):
		return self._capacity
		
	def has_room_for(self, thing):
		if self.capacity() == None:
			return True
		elif thing.volume() <= self.capacity():
			return True
		else:
			return False
	
	def action_add(self, thing):
		if self.has_room_for(thing) and self.is_open():
			self.add(thing)
			if self.capacity() != None:
				self.set_capacity(self.capacity() - thing.volume())
			return True
		else:
			return False
	
	def find(self, name):
		if self.is_open():
			for thing in self._contents:
				if thing.has_name(name):
					return True
			return False
		else:
			return None
