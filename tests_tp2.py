from tp2 import * 
from Thing import * 
import yaml

def test_Box_yaml():
	text = """
- is_open: true 
  capacity: 3 
"""
	text = yaml.load(text)
	b = Box.from_yaml(text)
	assert b.is_open() == True
	assert b.capacity() == 3

def test_Thing_yaml():
	text = """
- vol: 5
  name: Test
"""
	text = yaml.load(text)
	t = Thing.from_yaml(text)
	assert t.getName() == "Test"
	assert t.volume()  == 5

def test_box_create():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	assert "truc1" in b
	assert "truc3" not in b
	b.remove("truc1")
	assert "truc1" not in b
	assert b.is_open() == False
	b.open()
	assert b.is_open() == True
	b.close()
	assert b.is_open() == False
	
	assert b.action_look() == "la boite est fermee"
	b.open()
	assert b.action_look() == "la boite contient: truc2"
	b.add("truc3")
	assert b.action_look() == "la boite contient: truc2, truc3"

def test_place():
	b = Box()
	t = Thing(3)
	assert t.volume() == 3
	assert b.has_room_for(t) == True
	assert b.capacity() == None 
	b.set_capacity(5)
	assert b.capacity() == 5
	b.set_capacity(2)	
	assert b.has_room_for(t) == False

def test_action_add():
	b = Box()
	t = Thing(3)
	assert b.action_add(t) == False # car fermée
	b.open()
	assert b.action_add(t) == True # car place illimitée
	b.set_capacity(2)
	assert b.action_add(t) == False
	b.set_capacity(5)
	assert b.action_add(t) == True
	assert b.action_add(t) == False

def test_nom_chose():
	t = Thing(3)
	b = Box()
	b.open()
	assert t.has_name('bidule') == False
	t.set_name('bidule')
	assert repr(t) == "bidule"
	assert t.has_name('bidule') == True 
	b.action_add(t)
	assert b.find('bidule') == True
	assert b.find('chose') == False
	b.close()
	assert b.find('bidule') == None # car boite fermée

def test_constructeur_thing():
	t = Thing(3)
	assert t.name == None
	a = Thing(3, "nono")
	assert repr(a) == "nono"
