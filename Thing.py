class Thing:
	def __init__(self, volume, name=None):
		self.vol  = volume
		self.name = name
	
	@staticmethod
	def from_yaml(data):
		vol  = data[0].get('vol')
		name = data[0].get('name')
		return Thing(volume=vol, name=name)
	
	def __repr__(self):
		return self.getName()
	
	def getName(self):
		return self.name
	
	def volume(self):
		return self.vol
	
	def set_name(self, name):
		self.name = name

	def has_name(self, name):
		if self.name != None:
			if self.name == name:
				return True 
			else:
				return False
		else:
			return False 
	
